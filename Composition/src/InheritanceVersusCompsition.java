import composition.Person;
import composition.Phone;
import inheritance.Dog;

/**
 * Author: Jan de Rijke
 */
public class InheritanceVersusCompsition {
	public static void main(String[] args) {
		//inheritance
		Dog snoopy=new Dog("Snoopy",1563);
		//composition
		Person charlie = new Person('M',new Phone("00-11023"));
	}
}
