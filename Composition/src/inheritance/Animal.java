package inheritance;

/**
 * Author: Jan de Rijke
 */
public class Animal {
	String name;

	public Animal(String name) {
		this.name = name;
	}
}
