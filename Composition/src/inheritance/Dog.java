package inheritance;

/**
 * Author: Jan de Rijke
 */
public class Dog extends Animal{
	int chipNumber;

	public Dog(String name, int chipNumber) {
		super(name);
		this.chipNumber = chipNumber;
	}
}
