package be.kdg.oo.polymorphism.model;

public class Bus extends Vehicle {
    private int capacity;
    private int passengers;

    public Bus(int capacity) {
        this.capacity = capacity;
        this.passengers = 0;
    }

    @Override
    public void drive() {
        System.out.println("The wheels of a bus go round and round...");
    }

    public void boardPassengers(int numberOfPassengers) {
        if (passengers + numberOfPassengers <= capacity) {
            System.out.println("All aboard");
        } else {
            System.out.println("Sorry... Maximum capacity reached");
        }
    }
}
