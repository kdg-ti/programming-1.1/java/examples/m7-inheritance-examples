package inheritance.model;

public class Comic extends Book{
	private String artist;

	public Comic(String code,
	             String description,
	             double price,
	             String author,
	             String title,
	             String artist) {
		super(code, description, price, author, title);
		this.artist = artist;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}
}
