package inheritance.model;

import java.util.Objects;

public class Software extends Product {
    private String version;

    public Software(String code, String description, double price, String version) {
        super(code, description, price);
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    @Override
    public boolean equals(Object other) {
        if(other instanceof Software software) {
            return super.equals(software) && Objects.equals(version, software.version);
        } else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), version);
    }
}
